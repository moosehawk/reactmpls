import * as types from './types'
import * as messageActions from './message'
import * as auth from '../utils/auth'
import * as request from './request'
import { browserHistory } from 'react-router'

export function fetchUserStart() {
  return {
    type: types.USER_FETCH_START
  }
}

export function fetchUserSuccess(user) {
  return {
    type: types.USER_FETCH_SUCCESS,
    user
  }
}

export function fetchUserFail() {
  return {
    type: types.USER_FETCH_FAIL
  }
}

export function loginUserStart() {
  return {
    type: types.USER_LOGIN_START
  }
}

export function loginUserSuccess(user) {
  return {
    type: types.USER_LOGIN_SUCCESS,
    user
  }
}

export function loginUserFail() {
  return {
    type: types.USER_LOGIN_FAIL
  }
}

export function logoutUserSuccess() {
  return {
    type: types.USER_LOGOUT_SUCCESS
  }
}

export function registerUserStart() {
  return {
    type: types.USER_REGISTER_START
  }
}

export function registerUserSuccess() {
  return {
    type: types.USER_REGISTER_SUCCESS
  }
}

export function registerUserFail() {
  return {
    type: types.USER_REGISTER_FAIL
  }
}

export function forgotPasswordStart() {
  return {
    type: types.USER_FORGOTPASSWORD_START
  }
}

export function forgotPasswordSuccess() {
  return {
    type: types.USER_FORGOTPASSWORD_SUCCESS
  }
}

export function forgotPasswordFail() {
  return {
    type: types.USER_FORGOTPASSWORD_FAIL
  }
}

export function resetPasswordStart() {
  return {
    type: types.USER_RESETPASSWORD_START
  }
}

export function resetPasswordSuccess() {
  return {
    type: types.USER_RESETPASSWORD_SUCCESS
  }
}

export function resetPasswordFail() {
  return {
    type: types.USER_RESETPASSWORD_FAIL
  }
}

/**
 * Fetch user data from a local access token, or request a new access token
 * using a stored refresh token, if the access token is expired.
 */
export function fetchUser() {
  return (dispatch) => {
    dispatch(fetchUserStart())

    if (!auth.accessTokenIsExpired()) {
      const user = auth.getAccessTokenDecoded()
      dispatch(fetchUserSuccess(user))
      return Promise.resolve(user)
    }

    const refreshToken = auth.getRefreshToken()
    if (!refreshToken) {
      dispatch(fetchUserFail())
      return Promise.reject()
    }

    return request.USER_REFRESH(refreshToken)
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              auth.destroyTokens()
              dispatch(fetchUserFail())
            } else {
              auth.setAccessToken(json.access_token)
              dispatch(fetchUserSuccess(auth.getAccessTokenDecoded()))
            }

            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, fetchUserFail))
  }
}

/**
 * Login to a user using an email and password,
 * and set the access/refresh tokens in localStorage.
 */
export function loginUser(email, password) {
  return (dispatch) => {
    dispatch(loginUserStart())

    return request.USER_LOGIN({ email, password })
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(loginUserFail())
              dispatch(messageActions.addMessage(json.message, 'danger'))
            } else {
              auth.setAccessToken(json.access_token)
              auth.setRefreshToken(json.refresh_token)
              dispatch(loginUserSuccess(auth.getAccessTokenDecoded()))
            }

            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, loginUserFail))
  }
}

/**
 * Logout of the current user.
 */
export function logoutUser() {
  return (dispatch) => {
    const refreshToken = auth.getRefreshToken()
    auth.destroyTokens()
    dispatch(logoutUserSuccess())
    browserHistory.push('/')
    dispatch(messageActions.addMessage('You have been logged out.', 'success'))

    if (!refreshToken) return Promise.resolve()

    return request.USER_LOGOUT(refreshToken)
  }
}

/**
 * Register a new user.
 * If successful, re-direct to index with message.
 */
export function registerUser(email, password) {
  return (dispatch) => {
    dispatch(registerUserStart())

    return request.USER_REGISTER({ email, password })
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(registerUserFail())
              dispatch(messageActions.addMessage(json.message, 'danger'))
            } else {
              dispatch(registerUserFail())
              browserHistory.push('/')
              dispatch(messageActions.addMessage(json.message, 'success'))
            }

            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, registerUserFail))
  }
}

/**
 * Verify a user's account using email and verificationToken
 */
export function verifyUser(email, verificationToken) {
  return (dispatch) => {
    dispatch(messageActions.addMessage('Your account is being verified...', 'warning', 'fa fa-spinner fa-pulse fa-fw'))

    return request.USER_VERIFY({ email, verificationToken })
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(messageActions.addMessage(json.message, 'danger'))
            } else {
              browserHistory.push('/msg')
              dispatch(messageActions.addMessage(json.message, 'success'))
            }

            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch))
  }
}

/**
 * Send request to API that user forgot their password, and needs a password reset link
 */
export function forgotPassword(email) {
  return (dispatch) => {
    dispatch(forgotPasswordStart())

    return request.USER_FORGOT({ email })
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(forgotPasswordFail())
              dispatch(messageActions.addMessage(json.message, 'danger'))
            } else {
              dispatch(forgotPasswordSuccess())
              dispatch(messageActions.addMessage(json.message, 'success'))
            }
            
            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, forgotPasswordFail))
  }
}

/**
 * Change/reset password using email, a password reset token, and user's chosen new password.
 * Re-direct to login if successful.
 */
export function resetPassword(email, token, password) {
  return (dispatch) => {
    dispatch(resetPasswordStart())

    return request.USER_RESET({ email, token, password })
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(resetPasswordFail())
              dispatch(messageActions.addMessage(json.message, 'danger'))
            } else {
              dispatch(resetPasswordSuccess())
              browserHistory.push('/login')
              dispatch(messageActions.addMessage(json.message, 'success'))
            }
            
            return response
        })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, resetPasswordFail))
  }
}
