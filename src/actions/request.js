import authFetch from '../utils/authFetch'
import { actions as toastrActions } from 'react-redux-toastr'

const API = '/api/v0/'
const GET_OPTIONS = {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
}
const DELETE_OPTIONS = {
  method: 'DELETE',
  headers: {
    'Accept': 'application/json'
  }
}
const POST_OPTIONS = {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-type': 'application/json'
  }
}
const PUT_OPTIONS = {
  method: 'PUT',
  headers: {
    'Accept': 'application/json',
    'Content-type': 'application/json'
  }
}

export const CATEGORIES_GET_ALL = () => authFetch(API + 'categories', GET_OPTIONS)
export const CATEGORIES_GET_ONE = (id) => authFetch(API + 'categories/' + id, GET_OPTIONS)
export const CATEGORY_CREATE = (body) => authFetch(API + 'categories', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const CATEGORY_DELETE = (id) => authFetch(API + 'categories/' + id, DELETE_OPTIONS)
export const CATEGORY_PUT = (body) => authFetch(API + 'categories/' + body.id, { ...PUT_OPTIONS, body: JSON.stringify(body) })

export const TODOS_GET_ALL = () => authFetch(API + 'todos', GET_OPTIONS)
export const TODOS_GET_ONE = (id) => authFetch(API + 'todos/' + id, GET_OPTIONS)
export const TODO_CREATE = (body) => authFetch(API + 'todos', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const TODO_DELETE = (id) => authFetch(API + 'todos/' + id, DELETE_OPTIONS)
export const TODO_PUT = (body) => authFetch(API + 'todos/' + body.id, { ...PUT_OPTIONS, body: JSON.stringify(body) })

export const USER_FORGOT = (body) => fetch(API + 'auth/forgot', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const USER_LOGIN = (body) => fetch(API + 'auth/login', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const USER_LOGOUT = (token) => fetch(API + 'auth/logout', { method: 'POST', headers: { 'Accept': 'application/json', 'Authorization': 'Basic ' + token } })
export const USER_REFRESH = (token) => fetch(API + 'auth/refresh', { method: 'POST', headers: { 'Accept': 'application/json', 'Authorization': 'Basic ' + token } })
export const USER_REGISTER = (body) => fetch(API + 'auth/register', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const USER_RESET = (body) => fetch(API + 'auth/reset', { ...POST_OPTIONS, body: JSON.stringify(body) })
export const USER_VERIFY = (body) => fetch(API + 'auth/verify', { ...POST_OPTIONS, body: JSON.stringify(body) })

export const HANDLE_ERROR_DEFAULT = (error, dispatch, fail) => {
  dispatch(toastrActions.error('A network error has occurred'))
  if (fail) dispatch(fail())
  throw error
}
