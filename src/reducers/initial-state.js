export default {
  user: {
    info: {},
    isFetching: true,
    isLoggedIn: false,
    isLoggingIn: false,
    isRegistering: false,
    isResettingPassword: false,
    isForgettingPassword: false
  },
  categories: {
    isFetching: false,
    allIds: [],
    byId: {}
  },
  message: null,
  todos: {
    isFetching: false,
    allIds: [],
    byId: {}
  }
}
