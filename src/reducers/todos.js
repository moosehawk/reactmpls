import * as types from '../actions/types'
import initialState from './initial-state'
import dataUtils from '../utils/data'

export default function todosReducer(state = initialState.todos, action) {
  switch (action.type) {
    case types.TODO_CREATE_START:
      return {
        ...state,
        allIds: [ ...state.allIds, 'creating' ],
        byId: {
          ...state.byId,
          'creating': action.todo
        }
      }
    case types.TODO_CREATE_SUCCESS:
      return {
        ...state,
        allIds: [ ...state.allIds.filter(id => id !== 'creating'), action.todo.id ],
        byId: {
          ...dataUtils.removeKey(state.byId, 'creating'),
          [action.todo.id]: action.todo
        }
      }
    case types.TODO_CREATE_FAIL:
      return {
        ...state,
        allIds: state.allIds.filter(id => id !== 'creating'),
        byId: dataUtils.removeKey(state.byId, 'creating')
      }
    case types.TODOS_FETCH_START:
      return {
        ...state,
        isFetching: true
      }
    case types.TODO_DELETE_START:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.todo.id]: {
            ...state.byId[action.todo.id],
            isDeleting: true
          }
        }
      }
    case types.TODO_DELETE_SUCCESS:
      return {
        ...state,
        allIds: state.allIds.filter(id => id !== action.todo.id),
        byId: dataUtils.removeKey(state.byId, action.todo.id)
      }
    case types.TODO_DELETE_FAIL:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.todo.id]: dataUtils.removeKey(state.byId[action.todo.id], 'isDeleting')
        }
      }
    case types.TODO_EDIT_START:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.todo.id]: {
            ...action.todo,
            isEditing: true,
            old: { ...state.byId[action.todo.id] }
          }
        }
      }
    case types.TODO_EDIT_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.todo.id]: {
            ...dataUtils.removeKey(state.byId[action.todo.id], 'old'),
            isEditing: false
          }
        }
      }
    case types.TODO_EDIT_FAIL:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.todo.id]: state.byId[action.todo.id].old,
          isEditing: false
        }
      }
    case types.TODOS_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        allIds: action.todos.map(todo => todo.id),
        byId: dataUtils.arrayToObject(action.todos)
      }
    case types.TODOS_FETCH_FAIL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
