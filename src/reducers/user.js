import * as types from '../actions/types'
import initialState from './initial-state'

export default function userReducer(state = initialState.user, action) {
  switch (action.type) {
    case types.USER_FETCH_START:
      return {
        ...state,
        isFetching: true
      }
    case types.USER_FETCH_SUCCESS:
      return {
        ...state,
        info: action.user,
        isLoggedIn: true,
        isFetching: false
      }
    case types.USER_FETCH_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        isFetching: false
      }
    case types.USER_LOGIN_START:
      return {
        ...state,
        isLoggingIn: true
      }
    case types.USER_LOGIN_SUCCESS:
      return {
        ...state,
        info: action.user,
        isLoggedIn: true,
        isLoggingIn: false
      }
    case types.USER_LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        isLoggingIn: false
      }
    case types.USER_LOGOUT_SUCCESS:
      return {
        ...state,
        info: {},
        isLoggedIn: false
      }
    case types.USER_REGISTER_START:
      return {
        ...state,
        isRegistering: true
      }
    case types.USER_REGISTER_SUCCESS:
      return {
        ...state,
        isRegistering: false
      }
    case types.USER_REGISTER_FAIL:
      return {
        ...state,
        isRegistering: false
      }
    case types.USER_RESETPASSWORD_START:
      return {
        ...state,
        isResettingPassword: true
      }
    case types.USER_RESETPASSWORD_SUCCESS:
      return {
        ...state,
        isResettingPassword: false
      }
    case types.USER_RESETPASSWORD_FAIL:
      return {
        ...state,
        isResettingPassword: false
      }
    case types.USER_FORGOTPASSWORD_START:
      return {
        ...state,
        isForgettingPassword: true
      }
    case types.USER_FORGOTPASSWORD_SUCCESS:
      return {
        ...state,
        isForgettingPassword: false
      }
    case types.USER_FORGOTPASSWORD_FAIL:
      return {
        ...state,
        isForgettingPassword: false
      }
    default:
      return state
  }
};
