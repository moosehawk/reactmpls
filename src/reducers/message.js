import * as types from '../actions/types'
import initialState from './initial-state'

export default function messageReducer(state = initialState.message, action) {
  switch (action.type) {
    case types.MESSAGE_ADD:
      return {
        ...state,
        text: action.text,
        style: action.style,
        icon: action.icon
      }
    case types.MESSAGE_REMOVE:
      return null
    default:
      return state
  }
};
