import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import * as userActions from '../../actions/user'
import Header from './Header'

class HeaderContainer extends Component {
  constructor(props, context) {
    super(props, context)
    
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    /**
     * Force re-render of the navbar header so the correct
     * tab is highlighted when the route changes
     */
    let justLoaded = true
    browserHistory.listen((e) => {
      if (!justLoaded) return this.forceUpdate()

      justLoaded = false
    })
  }

  logout() {
    this.props.actions.logoutUser()
  }

  render() {
    return (
      <Header user={this.props.user} logout={this.logout} />
    )
  }
}

HeaderContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
}

function mapStateToProps(state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer)
