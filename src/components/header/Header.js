import React, { PropTypes } from 'react'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap'
import { Link } from 'react-router'

const Header = ({ logout, user }) => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">React Mpls</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <IndexLinkContainer to="/">
            <NavItem>Index</NavItem>
          </IndexLinkContainer>
          <LinkContainer to="todos">
            <NavItem>Todos</NavItem>
          </LinkContainer>
        </Nav>
        <Nav pullRight>
          {user.isLoggedIn ? (
            <NavDropdown eventKey={3} id="user-menu" title={user.info.email}>
              <MenuItem eventKey={3.1} onClick={logout}>Logout</MenuItem>
            </NavDropdown>
          ) : (
            <LinkContainer to="/login">
              <NavItem eventKey={4}>Login</NavItem>
            </LinkContainer>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
}

export default Header
