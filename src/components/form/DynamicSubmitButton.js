import React , { PropTypes } from 'react'
import { Button } from 'react-bootstrap'

const DynamicSubmitButton = ({ disabled, disabledText, text }) => {
  return disabled ? (
    <Button bsStyle="primary" type="submit" disabled>
      <i className="fa fa-spinner fa-pulse fa-fw"></i> {disabledText}
    </Button>
    ) : (
    <Button bsStyle="primary" type="submit">
      {text}
    </Button>
  )
}

DynamicSubmitButton.propTypes = {
  disabled: PropTypes.bool,
  disabledText: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
}

export default DynamicSubmitButton
