import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../../../actions/user'
import RegisterForm from './RegisterForm'

class RegisterPage extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      registrationForm: { email: '', password: '' }
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    return this.setState({
      registrationForm: Object.assign({}, this.state.registrationForm, {
        [e.target.name]: e.target.value
      })
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.userActions.registerUser(this.state.registrationForm.email, this.state.registrationForm.password)
  }

  render() {
    return (
      <RegisterForm
        form={this.state.registrationForm}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        submitDisabled={this.props.user.isRegistering}
      />
    )
  }
}

RegisterPage.propTypes = {
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage)
