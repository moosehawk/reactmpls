import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../../../actions/user'
import * as messageActions from '../../../actions/message'
import IndexPage from '../../index/IndexPage'

class VerifyPage extends Component {
  componentDidMount() {
    const queryParams = this.props.location.query
    
    if (!queryParams.email || !queryParams.verificationToken)
      return this.props.messageActions.addMessage('Could not verify your account. Please check your e-mail and try again.', 'danger')
    
    this.props.userActions.verifyUser(queryParams.email, queryParams.verificationToken)
  }

  render() {
    return (
      <IndexPage />
    )
  }
}

VerifyPage.propTypes = {
  messageActions: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    messageActions: bindActionCreators(messageActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(VerifyPage)
