import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../../../actions/user'
import ForgotForm from './ForgotForm'

class Forgot extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      email: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    return this.setState(Object.assign({}, this.state, {
      [e.target.name]: e.target.value
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.userActions.forgotPassword(this.state.email)
  }

  render() {
    return (
      <ForgotForm
        form={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        submitDisabled={this.props.user.isForgettingPassword}
      />
    )
  }
}

Forgot.propTypes = {
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Forgot)
