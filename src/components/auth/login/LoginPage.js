import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import { Link } from 'react-router'
import * as messageActions from '../../../actions/message'
import * as userActions from '../../../actions/user'
import LoginForm from './LoginForm'

class LoginPage extends Component {
  constructor(props, context) {
    super(props, context)
    
    this.state = {
      loginForm: { email: '', password: '' }
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    return this.setState({
      loginForm: Object.assign({}, this.state.loginForm, {
        [e.target.name]: e.target.value
      })
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.userActions.loginUser(this.state.loginForm.email, this.state.loginForm.password)
      .then((response) => {
        if (!response.ok) return
        browserHistory.push(this.props.location.state ? this.props.location.state.nextLocation.pathname : '/')
        this.props.messageActions.addMessage('You have logged in successfully', 'success')
      })
  }

  render() {
    return (
      <div>
        <Link to="register">Need an account?</Link>
        <LoginForm
          form={this.state.loginForm}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          submitDisabled={this.props.user.isLoggingIn}
        />
        <Link to="forgot">Forgot your password?</Link>
      </div>
    )
  }
}

LoginPage.propTypes = {
  messageActions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    messageActions: bindActionCreators(messageActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
