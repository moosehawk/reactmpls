import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import * as userActions from '../../../actions/user'
import * as messageActions from '../../../actions/message'
import ResetForm from './ResetForm'

class ResetPage extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      password: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    const queryParams = this.props.location.query

    if (!queryParams.email || !queryParams.token) {
      browserHistory.push('/login')
      this.props.messageActions.addMessage('Could not reset your password. Please check your e-mail and try again', 'danger')
    }
  }

  handleChange(e) {
    return this.setState(Object.assign({}, this.state, {
      [e.target.name]: e.target.value
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    const queryParams = this.props.location.query
    this.props.userActions.resetPassword(queryParams.email, queryParams.token, this.state.password)
  }

  render() {
    return (
      <ResetForm
        form={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        submitDisabled={this.props.user.isResettingPassword}
      />
    )
  }
}

ResetPage.propTypes = {
  messageActions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    messageActions: bindActionCreators(messageActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPage)
