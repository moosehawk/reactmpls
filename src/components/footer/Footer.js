import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="text-muted">
          <i className="fa fa-copyright"></i> 2016 Tim Arnal | <a href="https://gitlab.com/moosehawk/reactmpls" target="_blank">GitLab</a>
          <div className="pull-right"><a href="https://gitlab.com/moosehawk/reactmpls/blob/master/LICENSE" target="_blank">MIT License</a></div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
