import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import ReduxToastr from 'react-redux-toastr'
import HeaderContainer from '../header/HeaderContainer'
import Footer from '../footer/Footer'
import MessageContainer from '../message/MessageContainer'
import './App.css'

export class App extends Component {
  render() {
    return (
      <div>
        <ReduxToastr
          showCloseButton={false} />
        <HeaderContainer />
        <main>
          <div className="container">
            { this.props.message ? (<MessageContainer message={this.props.message} />) : null}
            {this.props.children}
          </div>
        </main>
        <Footer />
      </div>
    )
  }
}

App.propTypes = {
  children: PropTypes.object,
  message: PropTypes.object
}

function mapStateToProps(state) {
  return {
    message: state.message
  }
}

export default connect(mapStateToProps)(App)
