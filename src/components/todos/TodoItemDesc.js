import React, { PropTypes } from 'react'

const TodoItemDesc = ({ description, isDeleting, isEditing, title }) => {
  return (
    <div className="todoItemDesc">
      <h4>
        {title}
        { isDeleting || isEditing ? (
          <i className="fa fa-spinner fa-pulse fa-fw"></i>
        ) : null}
      </h4>
      {description}
    </div>
  )
}

TodoItemDesc.defaultProps = {
  isDeleting: false,
  isEditing: false
}

TodoItemDesc.propTypes = {
  description: PropTypes.string.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isEditing: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
}

export default TodoItemDesc
