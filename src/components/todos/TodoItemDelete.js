import React, { PropTypes } from 'react'

const TodoItemDelete = ({ isDeleting, remove }) => {
  return (
    <a href="#" className="btn delete" disabled={isDeleting} onClick={e => {
      e.preventDefault()
      if (!isDeleting) remove()
    }}>
      <i className="fa fa-close todoItemDelete"></i>
    </a>
  )
}

TodoItemDelete.defaultProps = {
  isDeleting: false
}

TodoItemDelete.propTypes = {
  isDeleting: PropTypes.bool.isRequired,
  remove: PropTypes.func.isRequired
}

export default TodoItemDelete
