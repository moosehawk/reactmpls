import React, { PropTypes } from 'react'
import { ListGroupItem } from 'react-bootstrap'
import TodoItemCheck from './TodoItemCheck'
import TodoItemDesc from './TodoItemDesc'
import TodoItemDelete from './TodoItemDelete'

const TodoItem = ({ todo, toggleComplete, remove }) => {
  return (
    <ListGroupItem className="todoItem">
      <div className="todoItemCheck pull-left">
        <TodoItemCheck
          completed={todo.completed}
          toggleComplete={toggleComplete} />
      </div>
      <div className="pull-left">
        <TodoItemDesc
          description={todo.description}
          isDeleting={todo.isDeleting}
          isEditing={todo.isEditing}
          title={todo.title} />
      </div>
      <div className="pull-right">
        <TodoItemDelete
          isDeleting={todo.isDeleting}
          remove={remove} />
      </div>
    </ListGroupItem>
  )
}

TodoItem.propTypes = {
  todo: PropTypes.object.isRequired,
  toggleComplete: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired
}

export default TodoItem
