import React from 'react'
import TodosList from './TodosList'

const TodosPage = () => {
  return (
    <div>
      <h1>To-do</h1>
      <TodosList />
    </div>
  )
}

TodosPage.propTypes = {
}

export default TodosPage
