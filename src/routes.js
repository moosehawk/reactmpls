import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './components/app/App'
import ForgotPage from './components/auth/forgot/ForgotPage'
import IndexPage from './components/index/IndexPage'
import LoginPage from './components/auth/login/LoginPage'
import RegisterPage from './components/auth/register/RegisterPage'
import ResetPage from './components/auth/reset/ResetPage'
import VerifyPage from './components/auth/verify/VerifyPage'
import TodosPage from './components/todos/TodosPage'

export default (store) => {
  function requireAuth(nextState, replace) {
    const state = store.getState()

    if (!state.user.isLoggedIn)
      return replace({
        pathname: '/login',
        state: {
          nextLocation: nextState.location
        }
      })
  }

  return (
    <Route path="/" component={App}>
      <IndexRoute component={IndexPage} />
      <Route path="todos" component={TodosPage} onEnter={requireAuth} />
      <Route path="login" component={LoginPage} />
      <Route path="register" component={RegisterPage} />
      <Route path="verify" component={VerifyPage} />
      <Route path="forgot" component={ForgotPage} />
      <Route path="reset" component={ResetPage} />
    </Route>
  )
}
