import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './store/store'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import routes from './routes'
import { fetchUser } from './actions/user'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import './index.css'
import 'react-redux-toastr/lib/css/react-redux-toastr.css'

const store = configureStore()

/**
 * Then .then().catch().then() syntax allows us to use the last
 * .then() as a "catch-all", so no matter if the fetchUser request fails
 * by network error, invalid refreshToken, or otherwise, it should always
 * display the application.
 */
store.dispatch(fetchUser()).then(() => {}).catch(() => {}).then(() => {
  ReactDOM.render(
    <Provider store={store}>
      <Router history={browserHistory}>
        { routes(store) }
      </Router>
    </Provider>,
    document.getElementById('root')
  )
})
