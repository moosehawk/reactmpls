import * as auth from './auth'
import * as userActions from '../actions/user'

export default function authFetch() {
  return (dispatch) => {
    if (!auth.accessTokenIsExpired()) return appendHeadersToFetch(...arguments)

    const refreshToken = auth.getRefreshToken()
    if (!refreshToken) {
      dispatch(userActions.logoutUser())
      return Promise.reject()
    }

    return fetch('/api/v0/auth/refresh', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Basic ' + refreshToken
      }
    })
    .then(response => {
      return response.json()
        .then(json => {
          if (!response.ok) {
            auth.destroyTokens()
            dispatch(userActions.logoutUser())
            let err = new Error()
            err.name = 'NotAuthenticated'
            err.message = 'You are not logged in'
            return Promise.reject(err)
          } else {
            auth.setAccessToken(json.access_token)
            return appendHeadersToFetch(...arguments)
          }
        })
    })
  }
}

function appendHeadersToFetch() {
  let args = [...arguments]
  
  const tokenHeaderValue = 'Bearer ' + auth.getAccessToken()
  if (args[1] && args[1].headers instanceof Headers) {
    args[1].headers.set('Authorization', tokenHeaderValue)
  } else if (args[1] && args[1].headers instanceof Object) {
    args[1].headers['Authorization'] = tokenHeaderValue
  } else {
    args[1] = {
      ...args[1],
      headers: { 'Authorization': tokenHeaderValue }
    }
  }
  
  return fetch(...args)
}
