let obj = {
  a: 'a value',
  b: 'b value'
}
console.log('obj', obj) // { a: 'a value', b: 'b value' }

obj.a = 'mutated a value'
console.log('obj', obj) // { a: 'mutated a value', b: 'b value' }

let newObj = {
  ...obj,
  a: 'new a value'
}

console.log('obj', obj) // { a: 'mutated a value', b: 'b value' }
console.log('newObj', newObj) // { a: 'new a value', b: 'b value' }
