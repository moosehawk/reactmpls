var bodyParser = require('body-parser');
var compression = require('compression');
var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var path = require('path');
var proxy = require('express-http-proxy');
var url = require('url');

require('dotenv').config();

var app = express();

app.use(morgan('dev'));
app.use(compression());
app.use(favicon(path.join(__dirname, 'public/img/favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', proxy(process.env.API_SERVER, {
  forwardPath: function(req, res) {
    return '/api' + url.parse(req.url).path;
  }
}));

/**
 * SPA route
 */
app.get('*', function(req, res) {
  var options = {
    root: __dirname + '/public/dist'
  };
  
  res.sendFile('index.html', options);
});

app.listen(process.env.PORT || 5000);
