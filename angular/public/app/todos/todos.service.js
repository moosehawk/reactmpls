(function() {
  'use strict';
  
  angular
    .module('app.todos')
    .service('todosService', todosService);
  
  todosService.$inject = ['$http', '$q', 'API'];
  /* @ngInject */
  function todosService($http, $q, API) {
    var vm = this;

    vm.createTodo = createTodo;
    vm.deleteTodo = deleteTodo;
    vm.editTodo = editTodo;
    vm.fetchTodos = fetchTodos;
    vm.todos = {
      isFetching: false,
      items: [],
      allIds: [],
      byId: {}
    };

    function createTodo(todo) {
      var creatingTodo = angular.copy(todo);
      var newTodoIndex = vm.todos.allIds.push('creating') - 1;
      vm.todos.byId['creating'] = todo;

      return $http.post(API.TODOS, todo).then(success, fail);

      function success(response) {
        var newTodo = response.data;
        vm.todos.allIds.splice(newTodoIndex, 1, newTodo.id);
        vm.todos.byId[newTodo.id] = newTodo;
        delete vm.todos.byId['creating'];
        return newTodo;
      }

      function fail(response) {
        vm.todos.allIds.splice(newTodoIndex, 1);
        delete vm.todos.byId['creating'];
        return $q.reject(creatingTodo);
      }
    }

    function deleteTodo(todo) {
      var todoState = vm.todos.byId[todo.id];

      todoState.isDeleting = true;
      return $http.delete(API.TODOS + '/' + todo.id).then(success, fail);

      function success(response) {
        delete vm.todos.byId[todo.id];
        vm.todos.allIds = vm.todos.allIds.filter(function(id) { return id !== todo.id; });
        return todoState = response.data;
      }

      function fail(response) {
        todoState.isDeleting = false;
        return $q.reject(response.msg);
      }
    }

    function editTodo(todo) {
      var todoState = vm.todos.byId[todo.id];
      var oldState = angular.copy(todoState);

      todoState.isEditing = true;
      return $http.put(API.TODOS + '/' + todo.id, todo).then(success, fail);

      function success(response) {
        todoState.isEditing = false;
        return todoState = response.data;
      }

      function fail(response) {
        todoState = oldState;
        return $q.reject(response.msg);
      }
    }

    function fetchTodos() {
      if (vm.todos.length > 1) return vm.todos;

      vm.todos.isFetching = true;
      return $http.get(API.TODOS).then(success, fail);

      function success(response) {
        vm.todos.isFetching = false;
        response.data.forEach(function(todo) {
          vm.todos.allIds.push(todo.id);
          vm.todos.byId[todo.id] = todo;
        });
        
        return vm.todos;
      }

      function fail(response) {
        vm.todos.isFetching = false;
        return $q.reject(response.msg);
      }
    }
  }
})();
