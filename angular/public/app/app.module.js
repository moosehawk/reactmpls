(function() {
	'use strict';
  
  angular
    .module('app', [
      /* Shared modules */
      'app.core',
      
      /* Feature areas */
      'app.auth',
      'app.index',
      'app.shell',
      'app.todos'
      
      /* 3rd-party modules */
  ]);
})();
