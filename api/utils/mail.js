import nodemailer from 'nodemailer'
import sendgrid from 'nodemailer-sendgrid-transport'

const apikey = {
  'development': process.env.SENDGRID_DEVELOPMENT,
  'test': process.env.SENDGRID_TEST,
  'production': process.env.SENDGRID_PRODUCTION
}

const transporter = nodemailer.createTransport(sendgrid({
  auth: {
    api_key: apikey[process.env.NODE_ENV]
  }
}), {
  from: 'do-not-reply@moosehawk.gitlab.io'
})

export default transporter
