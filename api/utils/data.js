export default {
  sanitizeHexColor,
  sanitzer
}

function sanitizeHexColor(color) {
  if (color && color.substr && color.substr(0, 1) !== '#') return '#' + color
  else return color
}

function sanitzer(data) {
  let safe = { ...data }
  delete safe.id
  delete safe.userEmail
  delete safe.createdAt
  delete safe.deletedAt
  return safe
}
