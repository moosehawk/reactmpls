export default {
  getUrl
}

/**
 * Parse a request and return the full host URL
 *
 * @param {object} req - Express request object
 */
function getUrl(req) {
  return req.protocol + '://' + req.get('host')
}
