import AppError from '../errors/AppError'

export default {
  routeError,
  routeNotFound
}

/**
 * Catch-all error route. Tries to return an appropriate error
 * based on what was caught, or 500 if it can't identify the source.
 */
function routeError(err, req, res, next) {
  /**
   * Express-validator error array
   */
  if (Array.isArray(err))
    return res.status(400).json({
      message: err[0].msg
    })
  
  /**
   * Error subclass of AppError
   */
  if (err instanceof AppError) {
    return res.status(err.status).json({
      message: err.message
    })
  }

  /**
   * Document not found
   */
  if (err.name === 'DocumentNotFoundError')
    return res.status(404).json({
      message: 'Item does not exist.'
    })

  /**
   * Generic error, generated with new Error('error text') but without a name
   */
  if (err.message)
    return res.status(400).json({
      message: err.message
    })
  
  /**
   * Catch-all, could not determine error type or source
   */
  return res.status(500).json({
    message: 'Something went wrong.'
  })
}

/**
 * Catch-all 404: no route found
 */
function routeNotFound(req, res) {
  return res.status(404).end()
}
