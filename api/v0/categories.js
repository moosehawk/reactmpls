import { Category } from '../db/models'
import { DocumentNotFoundError } from '../errors/data-errors'
import dataUtils from '../utils/data'

export default {
  create,
  get,
  getAll,
  remove,
  retrieve,
  retrieveOne,
  update,
  validate,
  validateId
}

/**
 * Create a new category
 */
function create(req, res, next) {
  Category.save({
    name: req.body.name,
    color: req.body.color,
    userEmail: req.user.email
  })
    .then(category => res.status(200).json(category))
    .catch(err => next(err))
}

/**
 * Respond with category matching id
 */
function get(req, res, next) {
  return res.status(200).json(req.store.category)
}

/**
 * Respond with all categories
 */
function getAll(req, res, next) {
  return res.status(200).json(req.store.categories)
}

/**
 * Delete the document from the DB
 */
function remove(req, res, next) {
  req.store.category.removeRelation('todos')
    .then(result => req.store.category.delete())
    .then(result => res.status(200).json({ message: 'Category deleted' }))
    .catch(err => next(err))
}

/**
 * Retrieve all categories for the authorized user
 */
function retrieve(req, res, next) {
  let params = { userEmail: req.user.email }
  if (req.params.categoryId) params.id = req.params.categoryId

  Category.filter({ ...params })
    .then(categories => {
      if (params.id && !categories[0]) {
        throw new DocumentNotFoundError()
      } else if (params.id) {
        req.store = { ...req.store, category: categories[0] }
      } else {
        req.store = { ...req.store, categories}
      }

      next()
    })
    .catch(err => next(err))
}

/**
 * Retrieve one category
 */
function retrieveOne(req, res, next) {
  let params = { userEmail: req.user.email }
  params.id = req.params.categoryId || req.body.categoryId

  if (!params.id) return next()

  Category.filter({ ...params })
    .then(categories => {
      if (!categories[0]) throw new DocumentNotFoundError('Category is invalid')
      
      req.store = { ...req.store, category: categories[0] }

      next()
    })
    .catch(err => next(err))
}

/**
 * Update a single category
 */
function update(req, res, next) {
  req.store.category.merge(req.body).save()
    .then(category => {
      return res.status(200).json(category)
    })
    .catch(err => next(err))
}

/**
 * Validate category body
 */
function validate(req, res, next) {
  req.checkBody('name', 'Category must have a name').notEmpty().isLength({ min: 1, max: undefined })
  if (req.body.color) {
    req.body.color = dataUtils.sanitizeHexColor(req.body.color)
    req.checkBody('color', 'Color must be a valid hex-color value').isHexColor()
  }
  const errors = req.validationErrors()
  if (errors) return next(errors)

  return next()
}

/**
 * Validate category ID
 */
function validateId(req, res, next) {
  req.checkParams('categoryId', 'Query must contain a valid category id').notEmpty().isUUID(4)
  const errors = req.validationErrors()
  if (errors) return next(errors)

  return next()
}
