import auth from './auth'
import dataUtils from '../utils/data'
import categories from './categories'
import error from './error'
import express from 'express'
import todos from './todos'

const router = express.Router()
const authRouter = express.Router()
const categoriesRouter = express.Router({ mergeParams: true })
const todosRouter = express.Router({ mergeParams: true })

/**
 * Authentication endpoints
 */
authRouter.post('/forgot', auth.validateEmail, auth.forgotPassword)
authRouter.post('/login', auth.validateEmail, auth.login)
authRouter.post('/refresh', auth.verifyRefreshToken, auth.refresh)
authRouter.post('/register', auth.validateEmail, auth.validatePassword, auth.register)
authRouter.post('/resend', auth.validateEmail, auth.resendVerification)
authRouter.post('/reset', auth.validateEmail, auth.validatePassword, auth.resetPassword)
authRouter.post('/verify', auth.validateEmail, auth.verifyEmailToken)
authRouter.post('/logout', auth.verifyRefreshToken, auth.logout)
authRouter.post('/logoutall', auth.verifyRefreshToken, auth.logoutAll)

/**
 * Category endpoints
 */
categoriesRouter.param('categoryId', categories.validateId)
categoriesRouter.post('/', categories.validate, categories.create)
categoriesRouter.get('/', categories.retrieve, categories.getAll)
categoriesRouter.get('/:categoryId', categories.retrieve, categories.get)
categoriesRouter.put('/:categoryId', categories.retrieve, categories.validate, categories.update)
categoriesRouter.delete('/:categoryId', categories.retrieve, categories.remove)
categoriesRouter.use('/:categoryId/todos', todosRouter)

/**
 * Todo endpoints
 */
todosRouter.param('todoId', todos.validateId)
todosRouter.post('/', categories.retrieveOne, todos.create)
todosRouter.get('/', todos.retrieve, todos.getAll)
todosRouter.get('/:todoId', todos.retrieve, todos.get)
todosRouter.put('/:todoId', todos.retrieve, todos.update)
todosRouter.delete('/:todoId', todos.retrieve, todos.remove)

router.use((req, res, next) => {
  req.store = {}
  req.body = dataUtils.sanitzer(req.body)

  // Delay processing all API calls for 1 second.
  // Helps illustrate more real-world waiting time on client.
  setTimeout(() => {
    next()
    // next(new Error('test'))  // this line can be used to automatically throw an error to all calls. Nice for testing.
  }, 1000)
})
router.use('/auth', authRouter)
router.use(auth.verifyAccessToken)
router.use('/categories', categoriesRouter)
router.use('/todos', todosRouter)

/**
 * Error handling
 */
router.use(error.routeError)
router.use(error.routeNotFound)

export default router
