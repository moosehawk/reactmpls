/**
 * Generic application error
 * Source: https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
 */
export default class AppError extends Error {
  constructor(message, status) {
    // Calling parent constrcutor of base Error class.
    super(message)

    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor)

    // Saving class name in the property of our custom error as a shortcut.
    this.name = this.constructor.name

    // You can use any additional properties you want.
    // I'm going to use preffered HTTP status for this error types.
    // `500` is the default value if not specified.
    this.status = status || 500
  }
}
