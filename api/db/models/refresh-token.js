import thinky from '../thinky'

const type = thinky.type
const schema = {
  token: type.string().uuid(4),
  expiration: type.date().required().default(Date.now() + 604800000),
  loggedOut: type.date(),
  userEmail: type.string().email().required(),
  createdAt: type.date().default(thinky.r.now()),
  updatedAt: type.date()
}
const options = {
  pk: 'token',
  enforce_extra: 'remove'
}
const RefreshToken = thinky.createModel('RefreshToken', schema, options)

export default RefreshToken

RefreshToken.define('extendExpiration', extendExpiration)
RefreshToken.define('isExpired', isExpired)
RefreshToken.define('logout', logout)
RefreshToken.pre('save', preSave)

/**
 * Extend the expiration date of the refresh token.
 * 604800000 = 1 week
 *
 * @param {callback} cb - Callback function
 * @returns {Promise}
 */
function extendExpiration(cb) {
  this.expiration = Date.now() + 604800000
  return this.save(cb)
}

/**
 * Determine if the refresh token is expired or logged out.
 *
 * @returns true if current datetime is beyond the expiration
 */
function isExpired() {
  return this.loggedOut ? true : Date.now() > this.expiration
}

/**
 * Force expire the refresh token.
 *
 * @param {callback} cb - Callback function
 * @returns {Promise}
 */
function logout(cb) {
  this.loggedOut = Date.now()
  return this.save(cb)
}

/**
 * Perform pre-save functions
 *
 * @param {function} next - Next callback
 */
function preSave(next) {
  this.updatedAt = thinky.r.now()
    
  next()
}
