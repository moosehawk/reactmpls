import thinky from '../thinky'

const type = thinky.type
const schema = {
  id: type.string().uuid(4), // auto-generated
  name: type.string().required(),
  color: type.string(),
  userEmail: type.string().email().required(),
  createdAt: type.date().default(thinky.r.now()),
  updatedAt: type.date()
}
const options = {
  enforce_extra: 'remove'
}
const Category = thinky.createModel('Category', schema, options)

export default Category

Category.pre('save', preSave)

/**
 * Pre-save function
 *
 * @param {function} next - Next callback
 */
function preSave(next) {
  this.updatedAt = thinky.r.now()

  next()
}
