import bcrypt from 'bcryptjs'
import thinky from '../thinky'
import uuid from 'node-uuid'
import validator from 'validator'

const type = thinky.type
const schema = {
  email: type.string().email().required(),
  password: type.string().required(),
  isVerified: type.date(),
  verificationToken: type.string().uuid(4).default(uuid.v4()),
  passwordResetToken: type.string().uuid(4),
  passwordResetExpiration: type.date(),
  createdAt: type.date().default(thinky.r.now()),
  updatedAt: type.date()
}
const options = {
  pk: 'email',
  enforce_extra: 'remove'
}
const User = thinky.createModel('User', schema, options)

export default User

User.define('comparePassword', comparePassword)
User.define('createPasswordResetToken', createPasswordResetToken)
User.define('isValidPasswordResetToken', isValidPasswordResetToken)
User.define('verifyEmail', verifyEmail)
User.defineStatic('hashPassword', hashPassword)
User.pre('save', preSave)

/**
 * Compare a plaintext password with a hashed password.
 *
 * @param {string} password - Plaintext password string to be compared
 * @param {callback} cb - Callback function
 */
function comparePassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, this.password, (err, matches) => {
      if (err) return reject(err)

      return resolve(matches)
    })
  })
}

/**
 * Generate a password reset token for this user
 * 604800000 = 1 week
 *
 * @param {callback} cb - Callback function
 * @returns {Promise}
 */
function createPasswordResetToken(cb) {
  this.passwordResetExpiration = Date.now() + 604800000
  this.passwordResetToken = uuid.v4()
  return this.save(cb)
}

/**
 * Hash a plaintext password using node-bcrypt
 *
 * @param {callback} cb - Callback function
 */
function hashPassword(password) {
  return new Promise((resolve, reject) => {
    if (!validator.isLength(password, { min: 8, max: 32 }))
      return reject(new Error('Password must be at least 8 characters.'))
    
    bcrypt.hash(password, 8, (err, hash) => {
      if (err) return reject(err)

      return resolve(hash)
    })
  })
}

/**
 * Compare a token to the password reset token, and ensure it's before the expiration date
 *
 * @param {string} token - Password reset token
 * @returns true if token is valid before expiration
 */
function isValidPasswordResetToken(token) {
  return this.passwordResetToken === token && this.passwordResetExpiration > Date.now()
}

/**
 * Perform pre-save functions
 *
 * @param {function} next - Next callback
 */
function preSave(next) {
  this.updatedAt = thinky.r.now()
    
  next()
}

/**
 * Set the user's account to verified
 *
 * @param {callback} cb - Callback function
 * @returns {Promise}
 */
function verifyEmail(cb) {
  this.isVerified = thinky.r.now()
  return this.save(cb)
}
