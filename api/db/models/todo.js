import thinky from '../thinky'

const type = thinky.type
const schema = {
  id: type.string().uuid(4), // auto-generated
  title: type.string(),
  description: type.string(),
  completed: type.boolean().default(false),
  userEmail: type.string().email().required(),
  categoryId: type.string(),
  createdAt: type.date().default(thinky.r.now()),
  updatedAt: type.date()
}
const options = {
  enforce_extra: 'remove'
}
const Todo = thinky.createModel('Todo', schema, options)

export default Todo

Todo.pre('save', preSave)

/**
 * Pre-save function
 *
 * @param {function} next - Next callback
 */
function preSave(next) {
  this.updatedAt = thinky.r.now()

  next()
}
