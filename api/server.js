import bodyParser from 'body-parser'
import dotenv from 'dotenv/config' // eslint-disable-line no-unused-vars
import customValidators from './errors/custom-validators'
import express from 'express'
import expressValidator from 'express-validator'
import helmet from 'helmet'
import morgan from 'morgan'
import v0 from './v0/_v0'

/**
 * Create express application
 */
const app = express()

/**
 * Middleware setup
 */
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(expressValidator({ customValidators }))
app.use(helmet())

/**
 * API routes
 */
app.use('/api/v0', v0)

/**
 * Server start
 */
app.listen(process.env.PORT || 4000)
